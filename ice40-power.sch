EESchema Schematic File Version 4
LIBS:ice40-mezzanine-card-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L gbtx:iCE40LP1K U6
U 1 1 5DE9843D
P 4150 3350
F 0 "U6" H 4075 4065 50  0000 C CNN
F 1 "iCE40LP1K" H 4075 3974 50  0000 C CNN
F 2 "gbtx:iCE40LP1K-CM121" H 5300 3350 50  0001 C CNN
F 3 "" H 5300 3350 50  0001 C CNN
	1    4150 3350
	1    0    0    -1  
$EndComp
Text HLabel 2300 2650 0    50   Input ~ 0
VCC1V2
Text HLabel 4800 4250 0    50   Input ~ 0
GND
Wire Wire Line
	4550 3000 4800 3000
Wire Wire Line
	4800 3000 4800 3100
Wire Wire Line
	4550 3100 4800 3100
Connection ~ 4800 3100
Wire Wire Line
	4800 3100 4800 3200
Wire Wire Line
	4550 3200 4800 3200
Connection ~ 4800 3200
Wire Wire Line
	4800 3200 4800 3300
Wire Wire Line
	4550 3300 4800 3300
Connection ~ 4800 3300
Wire Wire Line
	4800 3300 4800 3400
Wire Wire Line
	4550 3400 4800 3400
Connection ~ 4800 3400
Wire Wire Line
	4800 3400 4800 3500
Wire Wire Line
	4550 3500 4800 3500
Connection ~ 4800 3500
Wire Wire Line
	4800 3500 4800 3600
Wire Wire Line
	4550 3600 4800 3600
Connection ~ 4800 3600
$Comp
L Device:C_Small C5
U 1 1 5E125962
P 2650 2750
F 0 "C5" H 2742 2796 50  0000 L CNN
F 1 "1u" H 2742 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2650 2750 50  0001 C CNN
F 3 "~" H 2650 2750 50  0001 C CNN
	1    2650 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 5E125D48
P 3000 2750
F 0 "C7" H 3092 2796 50  0000 L CNN
F 1 "10n" H 3092 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3000 2750 50  0001 C CNN
F 3 "~" H 3000 2750 50  0001 C CNN
	1    3000 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 2650 2400 2650
Connection ~ 2650 2650
Wire Wire Line
	2650 2650 3000 2650
Wire Wire Line
	3000 2650 3450 2650
Wire Wire Line
	3450 2650 3450 3000
Wire Wire Line
	3450 3300 3600 3300
Connection ~ 3000 2650
Wire Wire Line
	3600 3200 3450 3200
Connection ~ 3450 3200
Wire Wire Line
	3450 3200 3450 3300
Wire Wire Line
	3600 3100 3450 3100
Connection ~ 3450 3100
Wire Wire Line
	3450 3100 3450 3200
Wire Wire Line
	3600 3000 3450 3000
Connection ~ 3450 3000
Wire Wire Line
	3450 3000 3450 3100
Wire Wire Line
	2650 2850 3000 2850
Wire Wire Line
	4800 3600 4800 4250
Text Notes 800  950  0    129  ~ 0
Power
Text HLabel 2300 3800 0    50   Input ~ 0
VCC2V5
$Comp
L Device:D_Small D1
U 1 1 5E1F1FDD
P 2700 3800
F 0 "D1" H 2700 3700 50  0000 C CNN
F 1 "~" H 2700 3686 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2700 3800 50  0001 C CNN
F 3 "~" V 2700 3800 50  0001 C CNN
	1    2700 3800
	-1   0    0    1   
$EndComp
NoConn ~ 3600 3900
$Comp
L Device:C_Small C6
U 1 1 5E2CC5CC
P 2950 3550
F 0 "C6" H 3042 3596 50  0000 L CNN
F 1 "10u" H 3042 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2950 3550 50  0001 C CNN
F 3 "~" H 2950 3550 50  0001 C CNN
	1    2950 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C8
U 1 1 5E2CC933
P 3300 3550
F 0 "C8" H 3392 3596 50  0000 L CNN
F 1 "100n" H 3392 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3300 3550 50  0001 C CNN
F 3 "~" H 3300 3550 50  0001 C CNN
	1    3300 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 2650 2400 3450
Connection ~ 2400 2650
Wire Wire Line
	2400 2650 2650 2650
$Comp
L Device:R_Small R4
U 1 1 5E2D7EBE
P 2700 3450
F 0 "R4" V 2504 3450 50  0000 C CNN
F 1 "100" V 2595 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2700 3450 50  0001 C CNN
F 3 "~" H 2700 3450 50  0001 C CNN
	1    2700 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	2400 3450 2600 3450
Wire Wire Line
	2800 3450 2950 3450
Connection ~ 2950 3450
Wire Wire Line
	2950 3450 3300 3450
Wire Wire Line
	2950 3650 3300 3650
Connection ~ 3300 3450
Connection ~ 3300 3650
Wire Wire Line
	3300 3650 3600 3650
Wire Wire Line
	3300 3450 3600 3450
Wire Wire Line
	2300 3800 2600 3800
Wire Wire Line
	2800 3800 3600 3800
$EndSCHEMATC
