EESchema Schematic File Version 4
LIBS:ice40-mezzanine-card-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L gbtx:iCE40LP1K U6
U 3 1 5DE9750A
P 3750 3250
F 0 "U6" H 3675 3965 50  0000 C CNN
F 1 "iCE40LP1K" H 3675 3874 50  0000 C CNN
F 2 "gbtx:iCE40LP1K-CM121" H 4900 3250 50  0001 C CNN
F 3 "" H 4900 3250 50  0001 C CNN
	3    3750 3250
	1    0    0    -1  
$EndComp
$Comp
L gbtx:M25P10-AVMN6 U7
U 1 1 5DD7ABF1
P 6200 3000
F 0 "U7" H 6300 3115 50  0000 C CNN
F 1 "M25P10-AVMN6" H 6300 3024 50  0000 C CNN
F 2 "gbtx:m25p10-avmn6" H 6200 3000 50  0001 C CNN
F 3 "" H 6200 3000 50  0001 C CNN
	1    6200 3000
	1    0    0    -1  
$EndComp
$Comp
L gbtx:Header6 U9
U 1 1 5DD7B2A0
P 8000 3100
F 0 "U9" H 8178 2846 50  0000 L CNN
F 1 "Header6" H 8178 2755 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x06_P1.00mm_Vertical" H 8000 3100 50  0001 C CNN
F 3 "" H 8000 3100 50  0001 C CNN
	1    8000 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5DD7D6AE
P 2350 2300
F 0 "C1" H 2442 2346 50  0000 L CNN
F 1 "1u" H 2442 2255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2350 2300 50  0001 C CNN
F 3 "~" H 2350 2300 50  0001 C CNN
	1    2350 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5DD7DB3B
P 2700 2300
F 0 "C2" H 2792 2346 50  0000 L CNN
F 1 "10n" H 2792 2255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2700 2300 50  0001 C CNN
F 3 "~" H 2700 2300 50  0001 C CNN
	1    2700 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5DD7DDDD
P 4850 2300
F 0 "C3" H 4942 2346 50  0000 L CNN
F 1 "1u" H 4942 2255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4850 2300 50  0001 C CNN
F 3 "~" H 4850 2300 50  0001 C CNN
	1    4850 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5DD7E133
P 5200 2300
F 0 "C4" H 5292 2346 50  0000 L CNN
F 1 "10n" H 5292 2255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5200 2300 50  0001 C CNN
F 3 "~" H 5200 2300 50  0001 C CNN
	1    5200 2300
	1    0    0    -1  
$EndComp
$Comp
L gbtx:Header2 U8
U 1 1 5DD82164
P 7500 1650
F 0 "U8" H 7728 1601 50  0000 L CNN
F 1 "Header2" H 7728 1510 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x02_P1.00mm_Vertical" H 7500 1650 50  0001 C CNN
F 3 "" H 7500 1650 50  0001 C CNN
	1    7500 1650
	1    0    0    -1  
$EndComp
Text HLabel 1900 2200 0    50   Input ~ 0
3V3
Wire Wire Line
	2350 2200 2700 2200
$Comp
L Device:R_Small R1
U 1 1 5DD9C178
P 5300 3050
F 0 "R1" H 5359 3096 50  0000 L CNN
F 1 "10k" H 5359 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5300 3050 50  0001 C CNN
F 3 "~" H 5300 3050 50  0001 C CNN
	1    5300 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5DD9C868
P 5550 3050
F 0 "R2" H 5609 3096 50  0000 L CNN
F 1 "10k" H 5609 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5550 3050 50  0001 C CNN
F 3 "~" H 5550 3050 50  0001 C CNN
	1    5550 3050
	1    0    0    -1  
$EndComp
Connection ~ 2700 2200
Wire Wire Line
	5200 2200 5800 2200
Wire Wire Line
	7450 2200 7450 3150
Wire Wire Line
	7450 3150 7800 3150
Connection ~ 5200 2200
Wire Wire Line
	5800 3150 5800 2550
Connection ~ 5800 2200
Wire Wire Line
	5800 2200 7450 2200
Wire Wire Line
	5550 2950 5550 2550
Wire Wire Line
	5550 2550 5800 2550
Connection ~ 5800 2550
Wire Wire Line
	5800 2550 5800 2200
Wire Wire Line
	5300 2950 5300 2550
Wire Wire Line
	5300 2550 5550 2550
Connection ~ 5550 2550
Wire Wire Line
	5200 2400 5700 2400
Wire Wire Line
	5700 2400 5700 3900
Wire Wire Line
	5700 3900 5950 3900
Connection ~ 5200 2400
Wire Wire Line
	2350 2400 2700 2400
Connection ~ 2700 2400
$Comp
L Device:R_Small R3
U 1 1 5DDA9315
P 7100 2850
F 0 "R3" H 7159 2896 50  0000 L CNN
F 1 "10k" H 7159 2805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7100 2850 50  0001 C CNN
F 3 "~" H 7100 2850 50  0001 C CNN
	1    7100 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3150 5550 3600
Wire Wire Line
	5550 3600 5950 3600
Wire Wire Line
	5950 3750 5300 3750
Wire Wire Line
	5300 3750 5300 3150
Text Label 2550 2850 0    50   ~ 0
iCE40_SI
Text Label 2550 3000 0    50   ~ 0
iCE40_SCK
Text Label 2550 3150 0    50   ~ 0
iCE40_SS
Wire Wire Line
	2550 3150 3050 3150
Wire Wire Line
	3050 3000 2550 3000
Wire Wire Line
	2550 2850 3050 2850
Wire Wire Line
	2700 2200 3000 2200
Wire Wire Line
	3050 3300 3000 3300
Wire Wire Line
	3000 3300 3000 2200
Connection ~ 3000 2200
Text Label 4750 2850 2    50   ~ 0
iCE40_SO
Wire Wire Line
	4750 2850 4300 2850
Wire Wire Line
	1900 2200 2350 2200
Connection ~ 2350 2200
Text Label 7050 3150 2    50   ~ 0
iCE40_SI
Wire Wire Line
	7050 3150 6650 3150
Wire Wire Line
	7100 2950 7100 3300
Wire Wire Line
	7100 3300 6650 3300
Wire Wire Line
	7100 2750 7100 2550
Wire Wire Line
	7100 2550 5800 2550
Text Label 4850 3300 0    50   ~ 0
iCE40_SO
Text Label 4850 3450 0    50   ~ 0
iCE40_SCK
Text Label 4850 3750 0    50   ~ 0
M25P10_SS
Wire Wire Line
	4850 3300 5950 3300
Wire Wire Line
	4850 3450 5950 3450
Wire Wire Line
	4850 3750 5300 3750
Connection ~ 5300 3750
Text Label 7300 3250 0    50   ~ 0
iCE40_SS
Text Label 7300 3350 0    50   ~ 0
iCE40_SO
Text Label 7300 3450 0    50   ~ 0
iCE40_SI
Text Label 7300 3550 0    50   ~ 0
iCE40_SCK
Wire Wire Line
	7300 3250 7800 3250
Wire Wire Line
	7300 3350 7800 3350
Wire Wire Line
	7300 3450 7800 3450
Wire Wire Line
	7300 3550 7800 3550
Text HLabel 5200 4200 0    50   Input ~ 0
GND
Wire Wire Line
	5200 4200 5700 4200
Wire Wire Line
	7500 4200 7500 3650
Wire Wire Line
	7500 3650 7800 3650
Wire Wire Line
	5700 3900 5700 4200
Connection ~ 5700 3900
Connection ~ 5700 4200
Wire Wire Line
	5700 4200 7500 4200
Text Label 6700 1700 0    50   ~ 0
iCE40_SS
Text Label 6700 1800 0    50   ~ 0
M25P10_SS
Wire Wire Line
	6700 1800 7350 1800
Wire Wire Line
	7350 1700 6700 1700
Text Notes 800  900  0    129  ~ 0
SPI and PROM
Wire Wire Line
	5950 3150 5800 3150
Connection ~ 4850 2200
Wire Wire Line
	4850 2200 5200 2200
Connection ~ 4850 2400
Wire Wire Line
	4850 2400 5200 2400
Wire Wire Line
	3000 2200 4850 2200
Wire Wire Line
	2700 2400 4850 2400
$EndSCHEMATC
